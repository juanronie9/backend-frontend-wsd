function buildChart(keys, values, charTitle) {
  let ctx = document.getElementById('myChart');
  let myChart = new Chart(ctx, {
    type: 'polarArea',
    data: {
      labels: keys,
      datasets: [{
        label: charTitle || 'My First Dataset',
        data: values,
        backgroundColor: [
          'rgb(26, 179, 148)',
          'rgb(123,117,117)',
          'rgb(248, 172, 89)',
          'rgb(237, 85, 101)',
        ]
      }]
    },
    options: {}
  });

  return myChart;
}

function buildTable(keys, values) {
  const myTable = new gridjs.Grid({
    columns: keys,
    search: true,
    pagination: true,
    sort: true,
    data: values,
  }).render(document.getElementById("wrapper"));

  return myTable;
}

$(document).ready(function() {

  $(document).ajaxSend(function() {
    $("#overlay").fadeIn(300);
  });

  $.ajax({
    url: "http://0.0.0.0:3000/v1/reports",
    data: {},
    success: function(result) {
      const { service_reports: sR } = result;

      const total200 = sR.reduce((acc, cur) => (cur.status_code && cur.status_code === 200 ? ++acc : acc), 0);
      const total403 = sR.reduce((acc, cur) => (cur.status_code && cur.status_code === 403 ? ++acc : acc), 0);
      const total501 = sR.reduce((acc, cur) => (cur.status_code && cur.status_code === 501 ? ++acc : acc), 0);
      const totalError = sR.reduce((acc, cur) => (
          cur.status_code && (cur.status_code === 500 || typeof cur.status_code === 'string')
              ? ++acc : acc), 0);

      // Chart
      const labels = [
          'OK', 'Not Implemented', 'Forbidden', 'Error'
      ];
      const data = [];
      data.push(total200);
      data.push(total501);
      data.push(total403);
      data.push(totalError);
      const charTitle = 'Service reports'
      buildChart(labels, data, charTitle);

      // Table
      const labelsTable = ['Status Code', 'Host Id', 'Host Name'];
      const dataTable = sR.map(element => {
        let result = [];
        result.push(element.status_code);
        result.push(element.host.id);
        result.push(element.host.name);
        return result;
      });
      buildTable(labelsTable, dataTable);

      // Summary
      $("#p200").html(total200);
      $("#p501").html(total501);
      $("#p403").html(total403);
      $("#pError").html(totalError);
    }
  }).done(function() {
    setTimeout(function(){
      $("#overlay").fadeOut(300);
    },300);
  });
});