const request = require('request');
const log4js = require('log4js');
const logger = log4js.getLogger('TokenAuth');
const HttpStatus = require('../../lib/HttpStatus');

class ReportsGateway {

    async requestStatusReport(token) {
        const uri = `https://staging-gateway.priipcloud.com/api/v2.0/gateway/reports/status/service`;
        const options = {
            method: 'POST',
            uri,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: {},
            json: true,
        };

        const res = await new Promise((resolve, reject) => {
            request(options, (err, response, body) => {
                if (err) {
                    logger.warn('Error during request status report: ', err);
                    return reject(err);
                }

                if (response.statusCode >= HttpStatus.BAD_REQUEST) {
                    reject(
                        body ||
                        new Error(
                            `Unable to request status report. ErrorCode: ${response.statusCode}`,
                        ),
                    );
                }

                return resolve(body.response || body);
            });
        });

        logger.debug('Successfully request: ', res);
        const { job_id: jobId } = res;
        return jobId;
    }

    async getStatusReport(token, jobId) {
        const uri = `https://staging-gateway.priipcloud.com/api/v2.0/gateway/reports/status/service/`+ jobId;
        const options = {
            method: 'GET',
            uri,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': 'application/json',
                'Accept': 'application/json',
            },
            body: {},
            json: true,
        };

        const res = await new Promise((resolve, reject) => {
            request(options, (err, response, body) => {
                if (err) {
                    logger.warn('Error during request status report: ', err);
                    return reject(err);
                }

                if (response.statusCode >= HttpStatus.BAD_REQUEST) {
                    reject(
                        body ||
                        new Error(
                            `Unable to get status report. ErrorCode: ${response.statusCode}`,
                        ),
                    );
                }
                return resolve(body);
            });
        });

        return res;
    }
}

module.exports = ReportsGateway;
