## BACKEND API: WSD service

#### Install and Run
```
npm install
node app.js
```
You can use Docker to run the API

#### Get service reports

`GET /v1/reports`

Returns all service status data

**Params:**

| Parameter      | Type    | Description                  | IsRequired | Location |
| -------------- | ------- | ---------------------------- | ---------- | -------- |

**Example**:

```
curl -X GET \
  http://0.0.0.0:3000/v1/reports \
          -H 'cache-control: no-cache' \
  -H 'postman-token: 22008222-ca45-fee2-6d6e-10a46b7d6fb9'
```
**Response:**

Body:
```
{
    "job_id": "61433bf11152001aea1707d7",
    "status": 200,
    "requested_at": "2021-09-16T12:43:29.278Z",
    "requested_by": "23b1f001-f1fb-4a22-8933-bb77b3265a71",
    "total_alerts": 101,
    "started_at": "2021-09-16T12:43:32.702Z",
    "completed_at": "2021-09-16T12:43:32.702Z",
    "service_reports": [
        {
            "status_code": 200,
            "status_text": "No web hosts registered.",
            "requested_at": "2021-09-16T12:43:29.362Z",
            "completed_at": "2021-09-16T12:43:29.362Z",
            "host": {
                "id": 149,
                "name": "Classic Web App Local Dev",
                "downstream_protocol": "http",
                "downstream_host": "localhost",
                "downstream_port": 3004,
                "upstream_protocol": "http",
                "upstream_host": "localhost",
                "upstream_port": 3004,
                "auth_schema_jwt": true,
                "auth_schema_oauth2": false,
                "enabled": true,
                "block_guest_access": false,
                "options": {
                    "failover_to": 0,
                    "ignore_scan": true,
                    "service_key": "https://classic",
                    "waf_enabled": false
                },
                "route_filters": []
            },
            "total_alerts": 0,
            "nodes": []
        },
    ]
}
```