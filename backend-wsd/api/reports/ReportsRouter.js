const {Router} = require('express');
const ReportsController = require('./ReportsController');
const router = Router();

router.get(
    '/',
    ReportsController.getStatusReports
);

module.exports = router;
