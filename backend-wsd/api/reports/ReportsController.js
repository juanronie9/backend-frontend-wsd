const ReportsGateway = require('../../modules/reports/ReportsGateway');
const HttpStatus = require('../../lib/HttpStatus');
const log4js = require('log4js');
const logger = log4js.getLogger('ReportsController');

class ReportsController {
    static async getStatusReports(req, res){

        try {
            if(!req.token){
                return res.status(HttpStatus.NOT_AUTHORIZED).json({errors: ['Not authorized']});
            }

            const reportsGateway = new ReportsGateway();
            const jobId = await reportsGateway.requestStatusReport(req.token);
            if (!jobId) {
                return res.status(HttpStatus.BAD_REQUEST).json({errors: ['JobId not valid']});
            }

            // Try to receive data from external api (not available immediately)
            // Wait 500ms and make a request. (10 times max). If not received data return error
            let response = null;
            for (let i=0; i<10; i++) {
                await new Promise(resolve => setTimeout(resolve, 500));
                response = await reportsGateway.getStatusReport(req.token, jobId);
                if (response) { break; }
            }

            if (!response) {
                return res.status(HttpStatus.BAD_REQUEST).json({errors: ['Data not available']});
            }

            return res.status(HttpStatus.OK).json(response);
        }
        catch( error ){
            logger.error('[getStatusReports()] Request failed: ', error);
            return res
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .json({ errors: ['Internal server error ']})
        }
    }
}

module.exports = ReportsController;
