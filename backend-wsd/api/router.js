const { Router } = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const TokenAuthenticator = require('../lib/TokenAuthenticator');
const reportsRouter = require('./reports/ReportsRouter');
const router = Router();
const checkAuth = TokenAuthenticator.getInstance().checkAuthMiddleware({});
router.use(cors());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use('/reports', checkAuth, reportsRouter);

router.get('/', (req, res)=>{
    return res.status(200).json({
        date: new Date()
    });
});

module.exports = router;
