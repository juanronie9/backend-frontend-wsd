module.exports = {
    httpPort: process.env.APP_HTTP_PORT || 3000,
    backendProvider: {
        clientId: process.env.APP_BACKEND_PROVIDER_CLIENT_ID || 'coding_test',
        clientSecret: process.env.APP_BACKEND_PROVIDER_CLIENT_SECRET || 'bwZm5XC6HTlr3fcdzRnD',
        uri: process.env.APP_BACKEND_PROVIDER_URI || 'https://staging-authentication.wallstreetdocs.com',
    },
    backendStatusReport: {
        uri: process.env.APP_BACKEND_STATUS_REPORT_URI || 'https://staging-gateway.priipcloud.com',
    },
    logging: {
        appenders: {
            out: { type: 'stdout', layout: { type: 'json', separator: ',' } }
        },
        categories: {
            default: {
                appenders: [ 'out' ],
                level: process.env.APP_LOG_LEVEL || 'all'
            }
        }
    }
};