const log4js = require('log4js');
const config = require('./config');

const ApplicationContainer = require('./lib/ApplicationContainer');
const TokenAuthenticator = require('./lib/TokenAuthenticator');
const apiRouter = require('./api/router');

log4js.addLayout('json', function(config) {
    return function(logEvent) { return JSON.stringify(logEvent) + config.separator; }
});
log4js.configure(config.logging);

const logger = log4js.getLogger('[app.js]');
const applicationContainer = ApplicationContainer.instance;

TokenAuthenticator
    .getInstance()
    .setBackendProviderUri(config.backendProvider.uri)
    .setBackendProviderClientId(config.backendProvider.clientId)
    .setBackendProviderClientSecret(config.backendProvider.clientSecret)

async function bootstrapApplication(){

    applicationContainer
        .setConfig(config)
        .useRouter('/v1', apiRouter);

    await applicationContainer.start(config.httpPort);

    logger.info('Bootstrap succeeded');
}

process.on('unhandledRejection', async (exception) =>{
    logger.fatal('Unhandled rejection. Shutdown application', exception);
    await applicationContainer.shutdown();
    process.exit(1);
});

bootstrapApplication();