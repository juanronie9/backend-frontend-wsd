const { Router } = require('express');
const express = require('express');
const log4js = require('log4js');
const logger = log4js.getLogger('Application');
const INSTANCE_PROP = Symbol('ApplicationContainer');

class ApplicationContainer {

    static get instance(){
        if(!this[INSTANCE_PROP]){
            this[INSTANCE_PROP] = new ApplicationContainer();
        }
        return this[INSTANCE_PROP];
    }

    get router(){
        if(!this._router){
            this._router = Router();
        }
        return this._router;
    }

    get models(){
        return this._models;
    }

    setModels(value){
        this._models = value;
        return this;
    }

    get expressApp(){
        if(!this._expressApp){
            this._expressApp = express();
        }
        return this._expressApp;
    }

    setExpressApp(value){
        this._expressApp = value;
        return this;
    }

    get config(){
        return this._config;
    }

    setConfig(value){
        this._config = value;
        return this;
    }

    /** Attach middleware to root router */
    useRouter(...args){
        return this.router.use(...args);
    }

    /** Attach middleware to app container */
    use(...args){
        return this.expressApp.use(...args);
    }

    start(httpPort = this.config.httpPort){
        this.expressApp.use(this.router);
        this._httpServer = this.expressApp.listen(httpPort);
        logger.info('Application is listening on %s', httpPort);
    }

    shutdown(){
        
        logger.info('Application is down');
    }
}

module.exports = ApplicationContainer;