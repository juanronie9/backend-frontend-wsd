const joi = require('@hapi/joi');
const HttpStatus = require('./HttpStatus');
const { Schema, ValidationOptions } = joi;
const { Request, Response, NextFunction, RequestHandler } = require('express');

const STRICT_VALIDATION = false;
const INSTANCE = Symbol('JoiRequestValidator');

/** Joi request validation helper */
class JoiRequestValidator {

    static getInstance(){
        if(!this[INSTANCE]){
            this[INSTANCE] = new this();
        }
        return this[INSTANCE];
    }
    
    validateQuery(schema) {
        return (req, res, next) => {
            const params = req.query;
            const { validated, errors } = this._validateRequest(params, schema);
            if (errors !== undefined)
                return res.status(HttpStatus.BAD_REQUEST).json({ errors });
            const raw = req.query;
            req.query = { ...validated, raw };
            return next();
        };
    }

    validateBody(schema) {
        return (req, res, next) => {
            const params = req.body;
            const { validated, errors } = this._validateRequest(params, schema);
            if (errors !== undefined)
                return res.status(HttpStatus.BAD_REQUEST).json({ errors });
            const raw = req.body;
            req.body = { ...validated, raw };
            return next();
        };
    }

    validateParams(schema) {
        return (req, res, next) => {
            const params = req.params;
            const { validated, errors } = this._validateRequest(params, schema, options);
            if (errors !== undefined)
                return res.status(HttpStatus.BAD_REQUEST).json({ errors });
            const raw = req.params;
            req.params = { ...validated, raw };
            return next();
        };
    }

    _validateRequest(params, schema) {
        const validationResult = joi.validate(params, schema, {
            abortEarly: false,
            allowUnknown: !STRICT_VALIDATION,
        });
        if (validationResult.error === null) {
            const validated = validationResult.value;
            return { validated };
        }
        const errors = validationResult.error.isJoi
            ? validationResult.error.details
            : [{ message: validationResult.error.message }];
        return { errors };
    }
}

module.exports = JoiRequestValidator;
