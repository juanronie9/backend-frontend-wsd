const request = require('request');
const log4js = require('log4js');
const logger = log4js.getLogger('TokenAuth');
const HttpStatus = require('./HttpStatus');

const INSTANCE = Symbol('TokenAuthenticator');

class TokenAuthenticator {

    static getInstance(){
        if(!this[INSTANCE]){
            this[INSTANCE] = new TokenAuthenticator();
        }
        return this[INSTANCE];
    }

    constructor(backendProviderUri) {
        this._backendProviderURI = backendProviderUri;
    }

    setBackendProviderUri(uri) { 
        this._backendProviderURI = uri;
        return this;
    }

    setBackendProviderClientId(clientId) {
        this._backendProviderClientId = clientId;
        return this;
    }

    setBackendProviderClientSecret(clientSecret) {
        this._backendProviderClientSecret = clientSecret;
        return this;
    }

    /** Creates Middleware that check authentication */
    checkAuthMiddleware() {
        return async (req, res, next) => {
            try {
                const token = await this.getOauthToken();
                if (!token) {
                    return res.status(HttpStatus.ACCESS_DENIED).json({ errors: ['Access denied'] });
                }
                req.token = token;
                return next();
            } catch (error) {
                return res.status(HttpStatus.ACCESS_DENIED).json({ errors: ['Access denied']});
            }
        };
    }

    async getOauthToken() {
        const uri = `${this._backendProviderURI}/oauth/token`;
        const grantType = 'client_credentials';
        const clientId = this._backendProviderClientId;
        const clientSecret = this._backendProviderClientSecret;
        const options = {
            method: 'POST',
            uri,
            body: {
                'grant_type': grantType,
                'client_id': clientId,
                'client_secret': clientSecret
            },
            json: true,
        };

        const res = await new Promise((resolve, reject) => {
            request(options, (err, response, body) => {
                if (err) {
                    logger.warn('Error during token validation: ', err);
                    return reject(err);
                }

                if (response.statusCode >= HttpStatus.BAD_REQUEST) {
                    reject(
                        body ||
                            new Error(
                                `Unable to get oauth token. ErrorCode: ${response.statusCode}`,
                            ),
                    );
                }
                return resolve(body.response || body);
            });
        });
        logger.debug('Successfully get oauth token. Token is', res);
        const { access_token: token } = res;
        return token;
    }
}

module.exports = TokenAuthenticator;
